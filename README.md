# http-extractor
# How to run test Kafka 
To start cluster use:
>docker run --rm -it \                                     
>           -p 2181:2181 -p 3030:3030 -p 8081:8081 \ 
>           -p 8082:8082 -p 8083:8083 -p 9092:9092 \ 
>          -e ADV_HOST=127.0.0.1 \ 
>           landoop/fast-data-dev

To get into console access to running Kafka:
>docker run --rm -it --net=host landoop/fast-data-dev bash

Examples of console consumer\producers usage:
>kafka-console-producer --broker-list 127.0.0.1:9092 --topic extractorUrls  
>kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic topic-A --from-beginning 

Web interface awalible here:

http://127.0.0.1:3030/