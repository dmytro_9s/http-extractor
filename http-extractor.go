package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/segmentio/kafka-go"
)

var writerForKafka *kafka.Writer
var readerForKafka *kafka.Reader

const _conf = "localhost:9092"
const _resultTopic = "extractorResults"
const _tasksTopic = "urlsTopic"

func main() {
	kafkaURL := flag.String("kafkaURL", _conf, "Kafka URL")
	resultTopic := flag.String("resultTopic", _resultTopic, "Topic for extraction results")
	tasksTopic := flag.String("urlsTopic", _tasksTopic, "Topic for urls to extract")

	fmt.Printf("Starting http extractor on Kafka server %s...\n", "")

	writerForKafka = kafka.NewWriter(kafka.WriterConfig{
		Brokers:  []string{*kafkaURL},
		Topic:    *resultTopic,
		Balancer: &kafka.LeastBytes{},
	})

	readerForKafka := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{*kafkaURL},
		//	GroupID:   "consumer-group-id",
		Topic:     *tasksTopic,
		Partition: 0,
		MinBytes:  10e3, // 10KB
		MaxBytes:  10e6, // 10MB
	})

	readerForKafka.SetOffset(42)
	ch := make(chan string)
	for {
		m, err := readerForKafka.ReadMessage(context.Background())
		if err != nil {
			break
		}
		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		//here we will try to read url's
		getURL := string(m.Value)
		fmt.Printf("\nattemting to GET %s \n", getURL)
		fmt.Printf("%s\n", getURL)
		go MakeRequest(getURL, ch)

	}
	readerForKafka.Close()

}
func writeToKafka(url string, body string) {
	writerForKafka.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(url),
			Value: []byte(body),
		},
	)

}

//MakeRequest do GETs on given url and put responce body in given channel
func MakeRequest(url string, ch chan<- string) {
	start := time.Now()
	resp, _ := http.Get(url)

	secs := time.Since(start).Seconds()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	responseString := string(body)
	fmt.Println(responseString)
	writeToKafka(url, responseString)
	ch <- fmt.Sprintf("%.2f elapsed with response length: %d %s\n", secs, len(body), url)
}
